const request 	= require('request');
const ip 		= require('ip');
const gpio 		= require('onoff').Gpio;
const fs 		= require('fs');
const needle = require('needle');
const cheerio = require('cheerio');
const server = require('./modules/server');
const controll = require('./modules/controll');


let ds18b20 = require('ds18b20');

let ds18b20_address;
ds18b20.sensors(function(err, ids) {
    console.log(ids);
    try{
            ds18b20_address = ids[0];
    }
    catch(err)
    {
            ds18b20_address = ids;
    }

});


    let Table = require('cli-table');
    let table = new Table({ head: ["", "Temp1", "Temp2", "Temp3", "HashRate", "Status"] });
//=-----------------------CONFIG----------------------------------------------------=
const ASIC_TURN_ON_TIMEOUT  = 35000;
const ASIC_TURN_OFF_TIMEOUT  = 35000;
const ASIC_MINER_SOFT_MODEL = "";

const ASIC_RELAY            = [{relay: new gpio(24, 'out'), port: 24}, 
                               {relay: new gpio(25, 'out'), port: 25}, 
                               {relay: new gpio(26, 'out'), port: 26}, 
                               {relay: new gpio(16, 'out'), port: 16}];

const USER = "root";
const PASSWORD = "root";

process.env.NOW_PWM = '0';
process.env.NOW_PUMP = "OFF";


process.env.DS18B20_TEMP = "-1";

const SERVER_PASSWORD = "12341234";

//=------------------------END------------------------------------------------------=

let template = {
    ip: "0.0.0.0",
    mac: "",
    temp: [-1, -1, -1],
    rate: -1,
    errors: [-1, -1, -1],
    workTime: -1,
	freq: -1,
	foundBlock: -1,
    status: "no signal",
    isMining: false,
    relay_index: -1
  };

template = JSON.stringify(template);

let BLOCK_RESULT = [];


let turnOffAll = () => {
	ASIC_RELAY.forEach((el, index) => {
		el.relay.write(1, () => {console.log("Disable "+index+" on GPIO "+el.port)});
  	});
};

let turnOnAll = () => {
  	ASIC_RELAY.forEach((el, index) => {
    	el.relay.write(0, () => {console.log("Enable "+index+" on GPIO "+el.port)});
  	});
};

const IP_PART = ip.address().split(".")[2];

let IP_LIST_OFF = [];
let IP_LIST_ON = [];
let IP_LIST_BLOCK = [];
let RUN = 0;

let count = 0;

let options = {
        url: null,
        method: "GET",
        timeout: 1000,
        'auth': {
          'user': USER,
          'pass': PASSWORD,
          'sendImmediately': false
            	}};

let scanned = 0;

let scanHost = (host, callback, num) => {
	options.url = "http://"+host + "/cgi-bin/get_system_info.cgi";
    request(options, (err, res, body) => {
    	if (!err && res.statusCode == 200)
    	{
    		try
    		{
    			let parsedJson = JSON.parse(body);
    			if(!IP_LIST_OFF.includes(host) && typeof parsedJson.IP != typeof undefined)
    			{
    				IP_LIST_OFF.push(host);
				console.log("ASIC "+host);
    			}
    		}
    		catch (err)
    		{
    			console.log(err);
    		}

    	}
    	console.log(host+" status code is "+(res?res.statusCode:"undefined"));
	scanned++;
	if(scanned == 256)
	{
		console.log("Going to callback");
		setTimeout(() => callback(num), 2000);
	}
   	});
};

let scanHostAndCompare = (host, callback, num) => {
	options.url = "http://"+host + "/cgi-bin/get_system_info.cgi";
    request(options, (err, res, body) => {
    	if (!err && res.statusCode == 200)
    	{
    		try
    		{
    			let parsedJson = JSON.parse(body);
    			if(!IP_LIST_OFF.includes(host))
    			{
					IP_LIST_BLOCK.push(host);
					BLOCK_RESULT.push(JSON.parse(template));
    				BLOCK_RESULT[BLOCK_RESULT.length-1].ip =  host;
    				BLOCK_RESULT[BLOCK_RESULT.length-1].mac = parsedJson.MAC;
    				BLOCK_RESULT[BLOCK_RESULT.length-1].relay_index = ASIC_RELAY[num];
				console.log("ASIC at "+host);
    			}
    		}
    		catch (err)
    		{
    			console.log(err);
    		}

    	}
 	console.log(host+" status code is "+(res?res.statusCode:"undefined"));
	scanned++;
        if(scanned == 256)
        {
		//ASIC_RELAY[num].relay.write(1, () => {console.log("Disable GPIO "+ASIC_RELAY[num].port)});
                console.log("Going to next ASCI");
                setTimeout(() => callback(++num), 2000);
        }
	});
};

let scanAllHostsOff = async (callback, num) => {
	turnOffAll();
	let emptyArray = new Array(256).fill(0);
	//console.log(emptyArray);
	setTimeout(() => {
		console.log("Scanning network...");
		emptyArray.forEach((el, index, arr) => {
			scanHost("192.168." + IP_PART + "." + index, callback, num);
		});
	}, ASIC_TURN_OFF_TIMEOUT);
};

let scanAllHostsAndCompare = (num, callback) => {
		console.log("Scanning network fot enabled ASIC...");
		let emptyArray = new Array(256).fill(0);
		emptyArray.forEach((el, index, arr) => {
			scanHostAndCompare("192.168." + IP_PART + "." + index,callback, num);
		});
};



let showAsic = (host, index) => {
    needle.get(host + '/cgi-bin/minerStatus.cgi', { username: USER, password: PASSWORD, auth: 'digest' }, (err, resp, body) => {
        if (err) {
            console.log(err);
        } else {
            const $ = cheerio.load(body);
            //try{

                let TEMP = $("#cbi-table-1-temp2");
				let temp = [
						TEMP[0].children[0].data,
						TEMP[1].children[0].data,
						TEMP[2].children[0].data
				];
				console.log(temp);

                let ghz = {
                    avg: $("#ant_ghsav").text(),
                    rt: $("#ant_ghs5s").text()
                };
				console.log(ghz);

                let foundBlock = $("#ant_foundblocks").text();
				console.log(foundBlock);

                let HW = $("cbi-table-1-hw");
				let hw = [
					HW[0].children[0].data,
					TEHWMP[1].children[0].data,
					HW[2].children[0].data
				];
				console.log(hw);

				BLOCK_RESULT[index].temp = temp;
				BLOCK_RESULT[index].rate = ghz;
				BLOCK_RESULT[index].errors = hw;
				BLOCK_RESULT[index].foundBlocks = foundBlock;
				
                updateTable(host, temp, ghz.rt, "OK");
				console.log(temp);
		//}
// 	catch(err){
// 	updateTable(host, ["no", "no", "no"], "no", "Not mining");
// 	BLOCK_RESULT[index].isMining = false;
// 	BLOCK_RESULT[index].status = "Not mining";
// 	console.log("Not mining");
// }
        }
    });
};

let updateTable = (host, temp, hr, status) => {
        let x = {};
        x[host] = [temp[0], temp[1], temp[2], hr, status];
        table.push(x);
};

let dropTable = () => {
    table = new Table({ head: ["", "Temp1", "Temp2", "Temp3", "HashRate", "Status"] });
};

let temp = (id, callback) => {
        ds18b20.temperature(id, function(err, value) {
			updateTable("ds18b20", [value, "------", "------"], "------", "------");
			process.env.DS18B20_TEMP = value;
			callback();
        });
}

let monitoring = () => {
	dropTable();
    //console.log("Monitor startet for "+IP_LIST_BLOCK.length+" asics");
	server.updateVal(BLOCK_RESULT);
    temp(ds18b20_address, () => {
    updateTable("PUMP", ["DUTY CICLE: ", process.env.NOW_PWM, "STATUS:"], process.env.NOW_PUMP, "------");
	//setTimeout(() => console.log(table.toString()), 5000);

	BLOCK_RESULT.forEach((el, index, arr) => {

		//console.log("ASIC with ip "+el);
		options.url = "http://"+el.ip+"/cgi-bin/get_system_info.cgi";
        	request(options, (err, res, body) => {
        		if (!err && res.statusCode == 200)
        		{
                		try
               	 		{
                        		let parsedJson = JSON.parse(body);
                        		console.log(parsedJson);
					           showAsic(el, index);
                		}
                		catch (err)
                		{
                        		console.log(err);
                                updateTable(el, [null, null, null], null, "error");
               	 		}
        		}
                else
                {
                    updateTable(el, [null, null, null], null, "error");
                }

			if(index == arr.length-1){
				setTimeout(() => monitoring(), 500);
				controll.action(BLOCK_RESULT);
				console.log('\033[2J');
				console.log(table.toString());
				return;
				}
        	});
	});
});
};

let scanAsicHost = (num) => {
	let bool = num < ASIC_RELAY.length;
	console.log(bool);
	if(bool)
	{
		scanned = 0;
		ASIC_RELAY[num].relay.write(0, () => {console.log("Enable GPIO "+ASIC_RELAY[num].port)});
		setTimeout(() => {
			scanAllHostsAndCompare(num, scanAsicHost);
		}, ASIC_TURN_ON_TIMEOUT);
	}
	else
	{
		console.log(BLOCK_RESULT);
		turnOnAll();
		console.log("Enabling monitor");
		setTimeout(() => {
			monitoring();
			server.start(SERVER_PASSWORD);
			console.log('\033[2J');
		}, 1000);
	}
};


scanAllHostsOff(scanAsicHost, 0);



