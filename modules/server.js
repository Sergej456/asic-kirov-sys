let express = require('express');
let app = express();
let needle = require('needle');
const basicAuth = require('express-basic-auth');

let BLOCKS;
let TEMP;
let PUMP;

exports.start = (password = '1234') => {

    app.use(basicAuth({
        users: { 'admin': password },
        challenge: true,
        realm: 'ASIC_ACCESS',
    }));

    app.get('/', (req, res) => {
        res.sendFile(__dirname + "/html/index.html");
    });

    app.get('/data', (req, res) => {
        res.type('json');

        let resJSON = {
            temp: process.env.DS18B20_TEMP,
            pump: process.env.NOW_PUMP,
            pump_pwm: process.env.NOW_PWM,
            asics: BLOCKS
        };

        //resJSON = JSON.stringify(resJSON);
        res.send(resJSON);
    });

    app.listen(3000, () => {
    console.log('Server started on port 3000');
    });
};

exports.updateVal = (newVal) => {
    BLOCKS = newVal;
};