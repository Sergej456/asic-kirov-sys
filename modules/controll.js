const pwm   = require('pigpio').Gpio;
const gpio  = require('onoff').Gpio;

const PUMP = new gpio(22, 'out');
const PWM_PIN = new pwm(4, {mode: pwm.OUTPUT});
let PUMP_DUTY = [0, 255/4, 255/3, 255/2, 255];
PWM_PIN.pwmWrite(PUMP_DUTY[0]);

exports.action = (blocks) => {
    blocks.forEach((el, index) => {
        let block_temp = Math.max(el.temp);
        if(block_temp > process.env.TEMP_STEP_1)
        {
            
        }
        else if(block_temp > process.env.TEMP_STEP_2)
        {
            
        }
        else if(block_temp > process.env.TEMP_STEP_3)
        {
            
        }
        else if(block_temp > process.env.TEMP_MAX)
        {
            
        }
        else
        {

        }
    });
};