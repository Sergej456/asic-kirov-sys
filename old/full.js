let request = require('request');
let ip = require('ip');
let gpio = require('onoff').Gpio;
let fs = require('fs');


//=-----------------------CONFIG----------------------------------------------------=
const ASIC_TURN_ON_TIMEOUT  = 60000;
const ASIC_MINER_SOFT_MODEL = "";

const ASIC_RELAY            = [new gpio(24, 'out'), 
                               new gpio(25, 'out'), 
                               new gpio(26, 'out'), 
                               new gpio(16, 'out')];


//=------------------------END------------------------------------------------------=


let turnOffAll = () => {
  ASIC_RELAY.forEach((el, index) => {
    el.write(1, () => {console.log("Disabled ASIC " + index)});
  });
console.log("ALL ASICS IS OFF");
};

let turnOnAll = () => {
  ASIC_RELAY.forEach((el, index) => {
    el.write(0, () => {console.log("Enabled ASIC " + index)});
  });
console.log("ALL ASICS IS ON");
};

const IP_PART = ip.address().split(".")[2];

let IP_LIST_OFF = [];
let IP_LIST_BLOCK = [];
let RUN = 0;

let count = 0;

let BLOCK_RESULT = [
  {
    ip: "0.0.0.0",
    mac: "",
    temp: [-1, -1, -1],
    rate: -1,
    errors: [-1, -1, -1],
    workTime: -1,
    freq: -1,
    status: "no signal",
    isMining: false,
    relay_index: -1
  },
  {
    ip: "0.0.0.0",
    mac: "",
    temp: [-1, -1, -1],
    rate: -1,
    errors: [-1, -1, -1],
    workTime: -1,
    freq: -1,
    status: "no signal",
    isMining: false,
    relay_index: -1
  },
  {
    ip: "0.0.0.0",
    mac: "",
    temp: [-1, -1, -1],
    rate: -1,
    errors: [-1, -1, -1],
    workTime: -1,
    freq: -1,
    status: "no signal",
    isMining: false,
    relay_index: -1
  },
  {
    ip: "0.0.0.0",
    mac: "",
    temp: [-1, -1, -1],
    rate: -1,
    errors: [-1, -1, -1],
    workTime: -1,
    freq: -1,
    status: "no signal",
    isMining: false,
    relay_index: -1
  }
];

let scanHost = (host, user, password) => {

      var options = {
        url: 'http://' + host + "/cgi-bin/get_system_info.cgi",
        method: "GET",
        timeout: 1000,
        'auth': {
          'user': user,
          'pass': password,
          'sendImmediately': false
                      }
              };

        request(options, (err, res, body) => {
          if (!err && res.statusCode == 200) 
          {
            if(RUN == 0)
		try{
              		IP_LIST_OFF.push(JSON.parse(body).ipaddress);
               } catch(err) { console.log(err);} 
	   if(RUN == 1)
		try{
              if(IP_LIST_OFF.includes(JSON.parse(body).ipaddress))
              {
                console.log("Found same ASIC");
              }
              else
              {
                let parsed = JSON.parse(body);
                IP_LIST_BLOCK.push({IP: parsed.ipaddress, MAC: parsed.macaddr});
              }
		} catch(err) { console.log(err);}
          }
          count++;
          console.log("count: " + count + " run: " + RUN);
          if(count == 0)
          {
            console.log("Start scanning...");
          }
          if(count == 255)
          {
            console.log("Scanning end");
            if(RUN == 0)
            {
                console.log(IP_LIST_OFF);
                RUN = 1;
                count = 0;
		turnOnAll();
                console.log("Waiting for 1 min to turn on ASICs");
                INTERVAL = setTimeout(()=>{
                  console.log("Start scanning second time...");
                  scanAll();
                }, ASIC_TURN_ON_TIMEOUT);
            }
            else if(RUN == 1)
            {
              console.log(IP_LIST_BLOCK);
              count = 0;
              RUN = 0;
              IP_LIST_BLOCK.forEach((el, index) => {
                BLOCK_RESULT[index].ip = el.IP;
                BLOCK_RESULT[index].mac = el.MAC;
              });
              console.log(BLOCK_RESULT);
              let finalString = JSON.stringify(BLOCK_RESULT);
              let fs = require('fs');
              fs.writeFile("/BLOCK_RESULT/result.json", finalString, (err) => {
                  if(err) {
                      return console.log(err);
                  }
                  console.log("BLOCK_RESULT/result.json file saved");
              }); 
              setTimeout(() => {launchMainLoop();}, 1000);
            }

          }
        });
};

let scanAll = () => 
{
  //turnOffAll();

setTimeout(() => {
 for(let i = 0; i < 255; i++)
  {
          scanHost("192.168." + IP_PART + "." + i, "root", "root");
  }
}, 20000);

};

    let checkIndex = (user, password, countIndex, asicNum) => {
    	
      console.log("Scanning asic number " + asicNum + " index: " + countIndex);

    	let awIndex = IP_LIST_BLOCK.length;
    	
      if(asicNum <= awIndex-1)
    	{

    	   let host = IP_LIST_BLOCK[asicNum].IP;
         ASIC_RELAY[countIndex].writeSync(1);

         setTimeout(() => {

        	   var options = {
                url: 'http://' + host + "/cgi-bin/get_system_info.cgi",
                method: "GET",
                timeout: 1000,
                'auth': {
                  'user': user,
                  'pass': password,
                  'sendImmediately': false
                              }
                      };

                request(options, (err, res, body) => {
        		      if(!err && res.statusCode == 200)
                  {
                     BLOCK_RESULT[asicNum].relay_index = countIndex;
                     console.log("Index found, index = " + countIndex);
                     ASIC_RELAY[countIndex].writeSync(0);
                     setTimeout(() => {
                      checkIndex("root", "root", 0, ++asicNum);
                     }, ASIC_TURN_ON_TIMEOUT);
                  }
                  else
                  {
                    console.log("no index found, next ");
                    ASIC_RELAY[countIndex].writeSync(0);
                    console.log("Turning on incorrect asic");
                    setTimeout(() => {
                      checkIndex("root", "root", --countIndex, asicNum);
                    }, ASIC_TURN_ON_TIMEOUT);
                  }
        	       });
              }, 5000);
    	}
    };

let launchMainLoop = () => {

	console.log("Checking index");
	checkIndex("root", "root", 3, 0);
};
turnOffAll();
scanAll();

