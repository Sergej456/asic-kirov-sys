var ds18b20 = require('ds18b20');

let temp = (id) => {
		ds18b20.temperature(id, function(err, value) {
  			console.log('Current temperature is', value);
			temp(id);
		});
}

ds18b20.sensors(function(err, ids) {
	temp(ids);
});
