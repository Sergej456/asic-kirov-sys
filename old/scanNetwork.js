var request = require('request');
var ip = require('ip');

const IP_PART = ip.address().split(".")[2];

let IP_LIST_OFF = [];
let IP_LIST_ON = [];
let RUN = 0;

let count = 0;

let INTERVAL;

let scanHost = (host, user, password) => {

      var options = {
        url: 'http://' + host + "/cgi-bin/get_system_info.cgi",
        method: "GET",
        timeout: 1000,
        'auth': {
          'user': user,
          'pass': password,
          'sendImmediately': false
                      }
              };

        request(options, (err, res, body) => {
          if (!err && res.statusCode == 200) 
          {
            if(RUN == 0)
              IP_LIST_OFF.push(JSON.parse(body).ipaddress);
            if(RUN == 1)
              if(IP_LIST_OFF.includes(JSON.parse(body).ipaddress))
              {
                console.log("Found same ASIC");
              }
              else
              {
                IP_LIST_ON.push(JSON.parse(body).ipaddress);
              }
          }
          count++;
          console.log("count: " + count + " run: " + RUN);
          if(count == 0)
          {
            console.log("Start scanning...");
          }
          if(count == 255)
          {
            console.log("Scanning end");
            if(RUN == 0)
            {
                console.log(IP_LIST_OFF);
                RUN = 1;
                count = 0;
                console.log("Waiting for 1 min to turn on ASICs");
                INTERVAL = setInterval(()=>{
                  console.log("Start scanning second time...");
                  scanAll();
                }, 60000);
            }
            else if(RUN == 1)
            {
              console.log(IP_LIST_ON);
              count = 0;
              RUN = 0;
              clearInterval(INTERVAL);
            }

          }
        });
};

let scanAll = () => 
{
  for(let i = 0; i < 255; i++)
  {
          scanHost("192.168." + IP_PART + "." + i, "root", "root");
  }  
};

scanAll();

