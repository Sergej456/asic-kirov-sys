const Gpio = require('pigpio').Gpio;

const led = new Gpio(4, {mode: Gpio.OUTPUT});

let dutyCycle = 255;
setInterval(()=>{
  led.pwmWrite(dutyCycle);
dutyCycle-=50;
if(dutyCycle <=0) dutyCycle=255;
}, 1000);
