
var request = require('request');
var ip = require('ip');

const IP_PART = ip.address().split(".")[2];

let callback = (err, res, body) => {
  if (!err && res.statusCode == 200) {
        console.log(JSON.parse(body).ipaddress);
  }
};

let scanHost = (host, user, password) => {

var options = {
  url: 'http://' + host + "/cgi-bin/get_system_info.cgi",
  method: "GET",
  'auth': {
    'user': user,
    'pass': password,
    'sendImmediately': false
  		}
	};

request(options, callback);
};

for(let i = 0; i < 255; i++)
{
	scanHost("192.168." + IP_PART + "." + i, "root", "root");
}

