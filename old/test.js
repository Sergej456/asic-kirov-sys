
const Scanner   =   require('local-network-scanner');
const Digest    =   require('digest-client');
const Cheerio   =   require('cheerio');
const Fs        =   require('fs');
const http      =   require('http');
const request   =   require("request");

let ips = [];

Array.prototype.contains = function ( needle ) {
   for (i in this) {
      if (this[i] == needle) return true;
   }
   return false;
}

Fs.readFile('ip.log', function(err, data) {
  if (err) 
    {
        console.log("No file");
        Fs.open('ip.log', 'w', function (err, file) {
          if (err) throw err;
          console.log("Created new file");
        });
        checkIp();
    }
    else
    {
        console.log("File OK");
        Fs.readFile('ip.log', 'utf-8', function(err, data) {
            let spl_data = data.split('\n');
            spl_data.pop();
            ips = spl_data;
            console.log(ips);
          });
    }
});



function checkIp()
{

    Scanner.scan(function(devices) {
        devices.forEach(function(el){

            console.log("Now scanning " + el.ip);
            checkHOST(el.ip, 100);
        });
    });
}


function checkHOST(host, timeout)
{
    request.get({  
        url: "http://" + host,
        timeout: timeout
    }, function (err, res, body) {
        if(typeof undefined == typeof body)
        {
            //console.log("NO!!!!!!!!!!!!!!!!!!");
        }
        else
        {
            //console.log("YES!!!!!!!!!!!!!!!!!!!");
            let digestClient = new Digest({username: 'root', password: 'root', https: false})
            digestClient.request({
                    host: host,
                    path: '/cgi-bin/get_system_info.cgi',
                    port: 80,
                    method: 'GET',
                    headers: { "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36" }
                    }, function (err, result) {
                    if (err) {
                        return console.log(err);
                    }
                    let $ = Cheerio.load(result);
                    if($.html() != "" && typeof $.html() != typeof undefined && !(ips.contains(host)))
                    {
                        ips.push(host);
                        console.log(ips);
                        Fs.appendFile('ip.log', host + "\n", function (err) {
                          if (err) throw err;
                          console.log(host + ' added to file');
                          if(result.indexOf("Ant") >= 0)
                            {
                                let json_obj = JSON.parse(result);
                                console.log("ASIC MINER FOUND");
                                console.log(json_obj);
                            }
                        });
                    }
                });
        }
    });
}
