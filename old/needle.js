const needle = require('needle');
const cheerio = require('cheerio');


let ASIC_BLOCK = [];

let checkAsic = (host, login, password, callback) => {
	needle.get(host + '/cgi-bin/get_system_info.cgi', { username: login, password: password, auth: 'digest' },
  	(err, resp, body) => {
		let result = JSON.parse(body);
		if(result.minertype == "Antminer S9")
			{
				if(!(ASIC_BLOCK.includes(host)))
					{
						ASIC_BLOCK.push(host);
					}
				callback();
			}
		else
			{
				callback();
			}
	});
}


checkAsic("192.168.1.41", "root", "root", () => {
	ASIC_BLOCK.forEach((el, index) => {
       	  	needle.get(el + '/cgi-bin/minerStatus.cgi', { username: "root", password: "root", auth: 'digest' }, (err, resp, body) => {
       	        	 const $ = cheerio.load(body);
			 let TEMP = $("#cbi-table-1-temp2");
        	         let temp = [
					TEMP[0].children[0].data, 
					TEMP[1].children[0].data, 
					TEMP[2].children[0].data
				    ];
			console.log(temp);
        	});
	});

});
