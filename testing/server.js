let server = require("../modules/server");

const SERVER_PASSWORD = "12341234";

server.start(SERVER_PASSWORD);

process.env.NOW_PWM = '0';
process.env.NOW_PUMP = "OFF";
process.env.DS18B20_TEMP = "-1";


let template = {
    ip: "0.0.0.0",
    mac: "",
    temp: [-1, -1, -1],
    rate: -1,
    errors: [-1, -1, -1],
    workTime: -1,
	freq: -1,
	foundBlock: -1,
    status: "no signal",
    isMining: false,
    relay_index: -1
  };
  template = JSON.stringify(template);

  let BLOCKS = [];

  for(let i = 0; i < 4; i++)
  {
      let x = JSON.parse(template);
      x.mac = Math.random();
      BLOCKS.push(x);
  }

setInterval(() => {
    server.updateVal(BLOCKS);
    BLOCKS = [];

  for(let i = 0; i < 4; i++)
  {
      let x = JSON.parse(template);
      x.mac = Math.random();
      BLOCKS.push(x);
  }
}, 500);
